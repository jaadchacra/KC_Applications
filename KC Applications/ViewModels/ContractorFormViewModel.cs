﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KC_Applications.Models;

namespace KC_Applications.ViewModels {
    public class ContractorFormViewModel {
        public IEnumerable<Region> Regions { get; set; }
        public IEnumerable<Expertise> Expertises { get; set; }
        public Contractor Contractor { get; set; }
    }
}