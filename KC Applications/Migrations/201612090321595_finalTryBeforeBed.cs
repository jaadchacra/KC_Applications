namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class finalTryBeforeBed : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RegionofCustomers",
                c => new
                    {
                        RegionofCustomerId = c.Byte(nullable: false),
                        RegionofCustomerName = c.String(),
                    })
                .PrimaryKey(t => t.RegionofCustomerId);
            
            CreateTable(
                "dbo.ServiceofKCs",
                c => new
                    {
                        ServiceofKCId = c.Int(nullable: false, identity: true),
                        ServiceTitle = c.String(),
                        ServiceDescription = c.String(),
                        CustomerFirstName = c.String(nullable: false, maxLength: 255),
                        CustomerLastName = c.String(nullable: false, maxLength: 255),
                        CustomerEmail = c.String(nullable: false),
                        CustomerNumber = c.Int(nullable: false),
                        RegionofCustomerId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceofKCId)
                .ForeignKey("dbo.RegionofCustomers", t => t.RegionofCustomerId, cascadeDelete: true)
                .Index(t => t.RegionofCustomerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceofKCs", "RegionofCustomerId", "dbo.RegionofCustomers");
            DropIndex("dbo.ServiceofKCs", new[] { "RegionofCustomerId" });
            DropTable("dbo.ServiceofKCs");
            DropTable("dbo.RegionofCustomers");
        }
    }
}
