namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExpertiseModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Expertises",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        ExpertiseName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Contractors", "ExpertiseId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Contractors", "ExpertiseId");
            AddForeignKey("dbo.Contractors", "ExpertiseId", "dbo.Expertises", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contractors", "ExpertiseId", "dbo.Expertises");
            DropIndex("dbo.Contractors", new[] { "ExpertiseId" });
            DropColumn("dbo.Contractors", "ExpertiseId");
            DropTable("dbo.Expertises");
        }
    }
}
