namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TempChangeRequiredContractorsForTesting : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Contractors", "ContractorLastName", c => c.String(maxLength: 255));
            AlterColumn("dbo.Contractors", "ContractorEmail", c => c.String());
            AlterColumn("dbo.Contractors", "ContractorPassword", c => c.String());
            AlterColumn("dbo.Contractors", "LinkedInPage", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contractors", "LinkedInPage", c => c.String(nullable: false));
            AlterColumn("dbo.Contractors", "ContractorPassword", c => c.String(nullable: false));
            AlterColumn("dbo.Contractors", "ContractorEmail", c => c.String(nullable: false));
            AlterColumn("dbo.Contractors", "ContractorLastName", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
