namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyServicestosolveFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Services", "ServiceAreaId", "dbo.ServRegions");
            DropIndex("dbo.Services", new[] { "ServiceAreaId" });
            CreateTable(
                "dbo.MyServices",
                c => new
                    {
                        MyServiceId = c.Int(nullable: false, identity: true),
                        ServiceTitle = c.String(),
                        ServiceDescription = c.String(),
                        CustomerFirstName = c.String(nullable: false, maxLength: 255),
                        CustomerLastName = c.String(nullable: false, maxLength: 255),
                        CustomerEmail = c.String(nullable: false),
                        CustomerNumber = c.Int(nullable: false),
                        ServiceAreaId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.MyServiceId)
                .ForeignKey("dbo.ServRegions", t => t.ServiceAreaId, cascadeDelete: true)
                .Index(t => t.ServiceAreaId);
            
            DropTable("dbo.Services");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        ServiceId = c.Int(nullable: false, identity: true),
                        ServiceTitle = c.String(),
                        ServiceDescription = c.String(),
                        CustomerFirstName = c.String(nullable: false, maxLength: 255),
                        CustomerLastName = c.String(nullable: false, maxLength: 255),
                        CustomerEmail = c.String(nullable: false),
                        CustomerNumber = c.Int(nullable: false),
                        ServiceAreaId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceId);
            
            DropForeignKey("dbo.MyServices", "ServiceAreaId", "dbo.ServRegions");
            DropIndex("dbo.MyServices", new[] { "ServiceAreaId" });
            DropTable("dbo.MyServices");
            CreateIndex("dbo.Services", "ServiceAreaId");
            AddForeignKey("dbo.Services", "ServiceAreaId", "dbo.ServRegions", "Id", cascadeDelete: true);
        }
    }
}
