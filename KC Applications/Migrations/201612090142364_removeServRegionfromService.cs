namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeServRegionfromService : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Services", name: "RegionId", newName: "ServiceAreaId");
            RenameIndex(table: "dbo.Services", name: "IX_RegionId", newName: "IX_ServiceAreaId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Services", name: "IX_ServiceAreaId", newName: "IX_RegionId");
            RenameColumn(table: "dbo.Services", name: "ServiceAreaId", newName: "RegionId");
        }
    }
}
