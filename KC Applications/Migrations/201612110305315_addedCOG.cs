namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCOG : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocalInventories", "COG", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocalInventories", "COG");
        }
    }
}
