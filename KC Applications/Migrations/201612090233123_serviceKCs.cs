namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class serviceKCs : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ServiceKCs",
                c => new
                    {
                        ServiceKCId = c.Int(nullable: false, identity: true),
                        ServiceTitle = c.String(),
                        ServiceDescription = c.String(),
                        CustomerFirstName = c.String(nullable: false, maxLength: 255),
                        CustomerLastName = c.String(nullable: false, maxLength: 255),
                        CustomerEmail = c.String(nullable: false),
                        CustomerNumber = c.Int(nullable: false),
                        AreaofCustomerId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceKCId)
                .ForeignKey("dbo.Regions", t => t.AreaofCustomerId, cascadeDelete: true)
                .Index(t => t.AreaofCustomerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceKCs", "AreaofCustomerId", "dbo.Regions");
            DropIndex("dbo.ServiceKCs", new[] { "AreaofCustomerId" });
            DropTable("dbo.ServiceKCs");
        }
    }
}
