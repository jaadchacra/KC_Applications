namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedInventories : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Inventories");
            AlterColumn("dbo.Inventories", "Id", c => c.Byte(nullable: false));
            AddPrimaryKey("dbo.Inventories", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Inventories");
            AlterColumn("dbo.Inventories", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Inventories", "Id");
        }
    }
}
