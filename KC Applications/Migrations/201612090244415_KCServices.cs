namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KCServices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.KCServices",
                c => new {
                    KCServiceId = c.Int(nullable: false, identity: true),
                    ServiceTitle = c.String(),
                    ServiceDescription = c.String(),
                    CustomerFirstName = c.String(nullable: false, maxLength: 255),
                    CustomerLastName = c.String(nullable: false, maxLength: 255),
                    CustomerEmail = c.String(nullable: false),
                    CustomerNumber = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.KCServiceId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.KCServices");
        }
    }
}
