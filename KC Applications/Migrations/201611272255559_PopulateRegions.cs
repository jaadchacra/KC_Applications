namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateRegions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        RegionName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Contractors", "RegionId", c => c.Byte(nullable: false));
            CreateIndex("dbo.Contractors", "RegionId");
            AddForeignKey("dbo.Contractors", "RegionId", "dbo.Regions", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contractors", "RegionId", "dbo.Regions");
            DropIndex("dbo.Contractors", new[] { "RegionId" });
            DropColumn("dbo.Contractors", "RegionId");
            DropTable("dbo.Regions");
        }
    }
}
