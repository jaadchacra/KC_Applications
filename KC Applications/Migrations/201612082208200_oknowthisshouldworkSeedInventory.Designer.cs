// <auto-generated />
namespace KC_Applications.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class oknowthisshouldworkSeedInventory : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(oknowthisshouldworkSeedInventory));
        
        string IMigrationMetadata.Id
        {
            get { return "201612082208200_oknowthisshouldworkSeedInventory"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
