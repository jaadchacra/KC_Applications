namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDatetoService : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceofKCs", "DateRequested", c => c.DateTime(nullable: false));
            AddColumn("dbo.ServiceofKCs", "UrgencyDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServiceofKCs", "UrgencyDate");
            DropColumn("dbo.ServiceofKCs", "DateRequested");
        }
    }
}
