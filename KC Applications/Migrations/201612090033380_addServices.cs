namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addServices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Services",
                c => new {
                    ServiceId = c.Int(nullable: false, identity: true),
                    ServiceTitle = c.String(),
                    ServiceDescription = c.String(),
                    CustomerFirstName = c.String(nullable: false, maxLength: 255),
                    CustomerLastName = c.String(nullable: false, maxLength: 255),
                    CustomerNumber = c.Int(nullable: false),

                })
                .PrimaryKey(t => t.ServiceId);
               
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Services", "RegionId", "dbo.Regions");
            DropIndex("dbo.Services", new[] { "RegionId" });
            DropTable("dbo.Services");
        }
    }
}
