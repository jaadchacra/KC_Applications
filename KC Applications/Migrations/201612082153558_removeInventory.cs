namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeInventory : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Inventories");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Inventories",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Price = c.Double(nullable: false),
                        NumberInStock = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
