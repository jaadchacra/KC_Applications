namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeLocalinv : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                 "dbo.LocalInventories",
                 c => new
                 {
                     Id = c.Byte(nullable: false),
                     InventoryName = c.String(),
                 })
                 .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
            
            
        }
    }
}
