namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TheRealPopulateRegions : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Regions (Id, RegionName) VALUES(1, 'Beirut')");
            Sql("INSERT INTO Regions (Id, RegionName) VALUES(2, 'Mount Lebanon')");
            Sql("INSERT INTO Regions (Id, RegionName) VALUES(3, 'North')");
            Sql("INSERT INTO Regions (Id, RegionName) VALUES(4, 'Beqaa')");
            Sql("INSERT INTO Regions (Id, RegionName) VALUES(5, 'Nabatieh')");
            Sql("INSERT INTO Regions (Id, RegionName) VALUES(6, 'South')");
        }
        
        public override void Down()
        {
        }
    }
}
