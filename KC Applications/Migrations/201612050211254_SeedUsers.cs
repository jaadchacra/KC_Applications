namespace KC_Applications.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class SeedUsers : DbMigration {
        public override void Up() {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'44835cd9-1d93-473a-928f-7219e99727c9', N'jxc02dev@gmail.com', 0, N'AP/X0JcxluavlI9QtV3vWw1m/DUrRKvMK81IXV05dHeVfOg0I0uTL5qC9gQMuvEtXw==', N'b9de897f-c079-4a45-9abb-52d929264a74', NULL, 0, 0, NULL, 1, 0, N'jxc02dev@gmail.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4f4b4358-84ae-4586-8a1a-0ed041debe21', N'admin@kcapplications.com', 0, N'ANoVT4svGgsT4wbwJwUDOAIcRjqBGDbeU7dLSOvEoru/3EWTt+jIb9MxDShvlORZDA==', N'5e4d9116-a9b4-416f-8c1a-28fd3934b4f3', NULL, 0, 0, NULL, 1, 0, N'admin@kcapplications.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'e157f63c-453c-414e-b463-1887990191d1', N'Admin')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'4f4b4358-84ae-4586-8a1a-0ed041debe21', N'e157f63c-453c-414e-b463-1887990191d1')

");
        }

        public override void Down() {
        }
    }
}
