namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dateApprovedService : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceofKCs", "DateApproved", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServiceofKCs", "DateApproved");
        }
    }
}
