namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRegionToContractorModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contractors", "Region", c => c.String(nullable: false));
            AlterColumn("dbo.Contractors", "ContractorName", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Contractors", "ContractorEmail", c => c.String(nullable: false));
            AlterColumn("dbo.Contractors", "ContractorPassword", c => c.String(nullable: false));
            AlterColumn("dbo.Contractors", "CV", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contractors", "CV", c => c.String());
            AlterColumn("dbo.Contractors", "ContractorPassword", c => c.String());
            AlterColumn("dbo.Contractors", "ContractorEmail", c => c.String());
            AlterColumn("dbo.Contractors", "ContractorName", c => c.String());
            DropColumn("dbo.Contractors", "Region");
        }
    }
}
