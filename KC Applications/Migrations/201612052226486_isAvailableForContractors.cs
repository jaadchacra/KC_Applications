namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isAvailableForContractors : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contractors", "isAccepted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contractors", "isAccepted");
        }
    }
}
