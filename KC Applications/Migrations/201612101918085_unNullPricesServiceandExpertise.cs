namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class unNullPricesServiceandExpertise : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Expertises", "ExpertisePrice", c => c.Double(nullable: false));
            AlterColumn("dbo.ServiceofKCs", "InventoryPrice", c => c.Double(nullable: false));
            AlterColumn("dbo.ServiceofKCs", "ExpertisePrice", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ServiceofKCs", "ExpertisePrice", c => c.Double());
            AlterColumn("dbo.ServiceofKCs", "InventoryPrice", c => c.Double());
            AlterColumn("dbo.Expertises", "ExpertisePrice", c => c.Double());
        }
    }
}
