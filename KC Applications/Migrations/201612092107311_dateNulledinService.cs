namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dateNulledinService : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ServiceofKCs", "DateRequested", c => c.DateTime());
            AlterColumn("dbo.ServiceofKCs", "UrgencyDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ServiceofKCs", "UrgencyDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ServiceofKCs", "DateRequested", c => c.DateTime(nullable: false));
        }
    }
}
