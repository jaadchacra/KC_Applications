namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class customerAreafix : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.KCServices", name: "KCCustomerAreaId", newName: "CustomerAreaId");
            RenameIndex(table: "dbo.KCServices", name: "IX_KCCustomerAreaId", newName: "IX_CustomerAreaId");
            AddColumn("dbo.CustomerAreas", "CustomerAreaName", c => c.String());
            DropColumn("dbo.CustomerAreas", "AreaName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CustomerAreas", "AreaName", c => c.String());
            DropColumn("dbo.CustomerAreas", "CustomerAreaName");
            RenameIndex(table: "dbo.KCServices", name: "IX_CustomerAreaId", newName: "IX_KCCustomerAreaId");
            RenameColumn(table: "dbo.KCServices", name: "CustomerAreaId", newName: "KCCustomerAreaId");
        }
    }
}
