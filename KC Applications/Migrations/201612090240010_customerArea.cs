namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class customerArea : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomerAreas",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        AreaName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CustomerAreas");
        }
    }
}
