namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class priceServiceAndExpertise : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Expertises", "ExpertisePrice", c => c.Double());
            AddColumn("dbo.ServiceofKCs", "InventoryPrice", c => c.Double());
            AddColumn("dbo.ServiceofKCs", "ExpertisePrice", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServiceofKCs", "ExpertisePrice");
            DropColumn("dbo.ServiceofKCs", "InventoryPrice");
            DropColumn("dbo.Expertises", "ExpertisePrice");
        }
    }
}
