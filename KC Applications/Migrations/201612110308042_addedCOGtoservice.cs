namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedCOGtoservice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceofKCs", "InventoryCOG", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServiceofKCs", "InventoryCOG");
        }
    }
}
