namespace KC_Applications.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class SeedExpertise : DbMigration {
        public override void Up() {
            Sql("INSERT INTO Expertises (Id, ExpertiseName) VALUES(1, 'Add/Upgrade Hardware')");
            Sql("INSERT INTO Expertises(Id, ExpertiseName) VALUES(2, 'Install/Upgrade Software')");
            Sql("INSERT INTO Expertises (Id, ExpertiseName) VALUES(3, 'Network Design and Implementation')");
            Sql("INSERT INTO Expertises (Id, ExpertiseName) VALUES(4, 'Troubleshoot Hardware and Software')");
            Sql("INSERT INTO Expertises (Id, ExpertiseName) VALUES(5, 'Software Training for Staff')");
            Sql("INSERT INTO Expertises (Id, ExpertiseName) VALUES(6, 'Hardware Training for Staff')");
     
        }

        public override void Down() {
        }
    }
}
