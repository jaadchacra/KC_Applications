namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEmailToServices : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "CustomerEmail", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Services", "CustomerEmail");
        }
    }
}
