namespace KC_Applications.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class SeedUsers2 : DbMigration {
        public override void Up() {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'20f8ee1b-7c3c-4724-8584-813712d653cf', N'jxc02@mail.aub.edu', 0, N'AGZ62wYJbcsx+5D2ckA6WPH0FvQRgzHJNpnQjnCImjJPvX/TMW1pOoMMU26dW2UibA==', N'b2000999-3bde-4298-a22f-f8ed02a1c02f', NULL, 0, 0, NULL, 1, 0, N'jxc02@mail.aub.edu')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c03bb8a7-eb33-4638-a989-4000bc9baf65', N'admin@kcapplications.com', 0, N'AHXoWT968KTkTV2f0AikUD30w4uOEynStmEDee+501q2P/caQ02hEp2wkV17tKzF+w==', N'2e6fad94-95ea-4f71-9932-4066c68c3b64', NULL, 0, 0, NULL, 1, 0, N'admin@kcapplications.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'854df0df-3724-4d55-a21e-236928031f25', N'Admin')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c03bb8a7-eb33-4638-a989-4000bc9baf65', N'854df0df-3724-4d55-a21e-236928031f25')



");
        }

        public override void Down() {
        }
    }
}
