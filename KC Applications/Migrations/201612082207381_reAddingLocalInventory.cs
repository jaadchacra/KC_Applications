namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class reAddingLocalInventory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocalInventories",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        LocalInventoryName = c.String(),
                        Price = c.Double(nullable: false),
                        NumberInStock = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LocalInventories");
        }
    }
}
