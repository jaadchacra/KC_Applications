namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requiredTitleandDescription : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ServiceofKCs", "ServiceTitle", c => c.String(nullable: false));
            AlterColumn("dbo.ServiceofKCs", "ServiceDescription", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ServiceofKCs", "ServiceDescription", c => c.String());
            AlterColumn("dbo.ServiceofKCs", "ServiceTitle", c => c.String());
        }
    }
}
