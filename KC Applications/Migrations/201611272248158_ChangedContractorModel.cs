namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedContractorModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contractors", "ContractorFirstName", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.Contractors", "ContractorLastName", c => c.String(nullable: false, maxLength: 255));
            AddColumn("dbo.Contractors", "LinkedInPage", c => c.String(nullable: false));
            DropColumn("dbo.Contractors", "ContractorName");
            DropColumn("dbo.Contractors", "Region");
            DropColumn("dbo.Contractors", "CV");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contractors", "CV", c => c.String(nullable: false));
            AddColumn("dbo.Contractors", "Region", c => c.String(nullable: false));
            AddColumn("dbo.Contractors", "ContractorName", c => c.String(nullable: false, maxLength: 255));
            DropColumn("dbo.Contractors", "LinkedInPage");
            DropColumn("dbo.Contractors", "ContractorLastName");
            DropColumn("dbo.Contractors", "ContractorFirstName");
        }
    }
}
