namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SmallChangeToContractorAndRegionModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contractors", "isContractorFreeToWork", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Regions", "RegionName", c => c.String(nullable: false));
            DropColumn("dbo.Contractors", "isFree");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contractors", "isFree", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Regions", "RegionName", c => c.String());
            DropColumn("dbo.Contractors", "isContractorFreeToWork");
        }
    }
}
