namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Orders : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocalInventory_Id = c.Byte(nullable: false),
                        ServiceofKC_ServiceofKCId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LocalInventories", t => t.LocalInventory_Id, cascadeDelete: true)
                .ForeignKey("dbo.ServiceofKCs", t => t.ServiceofKC_ServiceofKCId, cascadeDelete: true)
                .Index(t => t.LocalInventory_Id)
                .Index(t => t.ServiceofKC_ServiceofKCId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "ServiceofKC_ServiceofKCId", "dbo.ServiceofKCs");
            DropForeignKey("dbo.Orders", "LocalInventory_Id", "dbo.LocalInventories");
            DropIndex("dbo.Orders", new[] { "ServiceofKC_ServiceofKCId" });
            DropIndex("dbo.Orders", new[] { "LocalInventory_Id" });
            DropTable("dbo.Orders");
        }
    }
}
