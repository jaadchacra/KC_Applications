namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class statusToService : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceofKCs", "Status", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ServiceofKCs", "Status");
        }
    }
}
