namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addContractortoService : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ServiceofKCs", "ContractorId", c => c.Int());
            CreateIndex("dbo.ServiceofKCs", "ContractorId");
            AddForeignKey("dbo.ServiceofKCs", "ContractorId", "dbo.Contractors", "ContractorId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceofKCs", "ContractorId", "dbo.Contractors");
            DropIndex("dbo.ServiceofKCs", new[] { "ContractorId" });
            DropColumn("dbo.ServiceofKCs", "ContractorId");
        }
    }
}
