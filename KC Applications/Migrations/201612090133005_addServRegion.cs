namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addServRegion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ServRegions",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        RegionName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ServRegions");
        }
    }
}
