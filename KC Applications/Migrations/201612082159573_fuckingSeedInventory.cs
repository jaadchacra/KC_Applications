namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fuckingSeedInventory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocalInventories", "Price", c => c.Double(nullable: false));
            AddColumn("dbo.LocalInventories", "NumberInStock", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocalInventories", "NumberInStock");
            DropColumn("dbo.LocalInventories", "Price");
        }
    }
}
