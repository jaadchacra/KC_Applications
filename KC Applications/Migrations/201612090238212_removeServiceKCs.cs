namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeServiceKCs : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ServiceKCs", "AreaofCustomerId", "dbo.Regions");
            DropIndex("dbo.ServiceKCs", new[] { "AreaofCustomerId" });
            DropTable("dbo.ServiceKCs");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ServiceKCs",
                c => new
                    {
                        ServiceKCId = c.Int(nullable: false, identity: true),
                        ServiceTitle = c.String(),
                        ServiceDescription = c.String(),
                        CustomerFirstName = c.String(nullable: false, maxLength: 255),
                        CustomerLastName = c.String(nullable: false, maxLength: 255),
                        CustomerEmail = c.String(nullable: false),
                        CustomerNumber = c.Int(nullable: false),
                        AreaofCustomerId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ServiceKCId);
            
            CreateIndex("dbo.ServiceKCs", "AreaofCustomerId");
            AddForeignKey("dbo.ServiceKCs", "AreaofCustomerId", "dbo.Regions", "Id", cascadeDelete: true);
        }
    }
}
