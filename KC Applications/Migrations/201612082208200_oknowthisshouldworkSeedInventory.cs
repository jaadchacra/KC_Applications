namespace KC_Applications.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class oknowthisshouldworkSeedInventory : DbMigration {
        public override void Up() {
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(1, 'HDD', 500000, 50)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(2, '4GB RAM', 150000, 40)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(3, '8GB RAM', 250000, 35)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(4, '16GB RAM', 400000, 45)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(5, 'Expansion card', 500000, 20)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(6, 'Printer', 500000, 12)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(7, 'E-commerce App', 13000000, 9999999)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(8, 'Social App', 7500000, 9999999)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(9, 'LMS App', 9000000, 9999999)");
            Sql("INSERT INTO LocalInventories (Id, LocalInventoryName, Price, NumberInStock) VALUES(10, 'Clothes Display App', 6000000, 9999999)");

        }

        public override void Down() {
        }
    }
}
