namespace KC_Applications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class solvingFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MyServices", "ServiceAreaId", "dbo.ServRegions");
            DropIndex("dbo.MyServices", new[] { "ServiceAreaId" });
            DropTable("dbo.MyServices");
            DropTable("dbo.ServRegions");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ServRegions",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        RegionName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MyServices",
                c => new
                    {
                        MyServiceId = c.Int(nullable: false, identity: true),
                        ServiceTitle = c.String(),
                        ServiceDescription = c.String(),
                        CustomerFirstName = c.String(nullable: false, maxLength: 255),
                        CustomerLastName = c.String(nullable: false, maxLength: 255),
                        CustomerEmail = c.String(nullable: false),
                        CustomerNumber = c.Int(nullable: false),
                        ServiceAreaId = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.MyServiceId);
            
            CreateIndex("dbo.MyServices", "ServiceAreaId");
            AddForeignKey("dbo.MyServices", "ServiceAreaId", "dbo.ServRegions", "Id", cascadeDelete: true);
        }
    }
}
