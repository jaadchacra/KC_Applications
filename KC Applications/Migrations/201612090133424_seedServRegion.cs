namespace KC_Applications.Migrations {
    using System;
    using System.Data.Entity.Migrations;

    public partial class seedServRegion : DbMigration {
        public override void Up() {
            Sql("INSERT INTO ServRegions (Id, RegionName) VALUES(1, 'Beirut')");
            Sql("INSERT INTO ServRegions (Id, RegionName) VALUES(2, 'Mount Lebanon')");
            Sql("INSERT INTO ServRegions (Id, RegionName) VALUES(3, 'North')");
            Sql("INSERT INTO ServRegions (Id, RegionName) VALUES(4, 'Beqaa')");
            Sql("INSERT INTO ServRegions (Id, RegionName) VALUES(5, 'Nabatieh')");
            Sql("INSERT INTO ServRegions (Id, RegionName) VALUES(6, 'South')");
        }

        public override void Down() {
        }
    }
}
