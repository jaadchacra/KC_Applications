﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KC_Applications.Models;

namespace KC_Applications.ViewModels {
    public class ServiceofKCFormViewModel {
        public IEnumerable<RegionofCustomer> RegionofCustomers { get; set; }
        public ServiceofKC ServiceofKC { get; set; }

    }
}


