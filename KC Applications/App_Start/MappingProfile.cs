﻿
using AutoMapper;
using KC_Applications.Models;
using KC_Applications.Dtos;

namespace KC_Applications.App_Start {
    public class MappingProfile : Profile {
        public MappingProfile() {

            //Domain to Dto

            //The added part resolves error in PUT when we make contractor.ContractorId = contractorDto.ContractorId
            Mapper.CreateMap<Contractor, ContractorDto>();
            Mapper.CreateMap<Region, RegionDto>();
            Mapper.CreateMap<Expertise, ExpertiseDto>();
            Mapper.CreateMap<ServiceofKC, ServiceofKCDto>();
            Mapper.CreateMap<RegionofCustomer, RegionofCustomerDto>();
            Mapper.CreateMap<LocalInventory, LocalInventoryDto>();


            //Dto to Domain
            Mapper.CreateMap<ContractorDto, Contractor>().ForMember(c => c.ContractorId, opt => opt.Ignore());
            Mapper.CreateMap<ServiceofKCDto, ServiceofKC>().ForMember(c => c.ServiceofKCId, opt => opt.Ignore());
            Mapper.CreateMap<LocalInventoryDto, LocalInventory>().ForMember(c => c.Id, opt => opt.Ignore());
        }


    }
}