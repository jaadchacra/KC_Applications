﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KC_Applications.Models {
    public class Order {
        public int Id { get; set; }

        [Required]
        public LocalInventory LocalInventory { get; set; }

        [Required]
        public ServiceofKC ServiceofKC { get; set; }


    }
}