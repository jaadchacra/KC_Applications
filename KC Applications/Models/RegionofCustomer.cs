﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KC_Applications.Models {
    public class RegionofCustomer {
        public byte RegionofCustomerId { get; set; }

        public string RegionofCustomerName { get; set; }
    }
}