﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace KC_Applications.Models {
    public class ServiceofKC {
        public int ServiceofKCId { get; set; }

        public Contractor Contractor { get; set; }
        public int? ContractorId { get; set; }

        [Required]
        [Display(Name = "Title")]
        public string ServiceTitle { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string ServiceDescription { get; set; }

        public string Status { get; set; }

        public double InventoryPrice { get; set; }

        public double InventoryCOG { get; set; }

        public double ExpertisePrice { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "First Name")]
        public string CustomerFirstName { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Last Name")]
        public string CustomerLastName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "E-mail")]
        public string CustomerEmail { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        public int CustomerNumber { get; set; }


        [Display(Name = "Region to Work in")]
        public RegionofCustomer RegionofCustomer { get; set; }
        [Display(Name = "Region to Work in")]
        public byte RegionofCustomerId { get; set; }

        public DateTime? DateRequested { get; set; }

        public DateTime? DateApproved { get; set; }

        [Display(Name = "If urgent, please specify when you need the contractor")]
        public DateTime? UrgencyDate { get; set; }
    }
}