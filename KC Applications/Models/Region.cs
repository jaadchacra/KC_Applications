﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KC_Applications.Models {
    public class Region {
        //[Required]
        public byte Id { get; set; }
        
        public string RegionName { get; set; }
    }
}