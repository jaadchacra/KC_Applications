﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KC_Applications.Models {
    public class EmailFormModel {
        [Required, Display(Name = "Name")]
        public string FromName { get; set; }
        [Required, Display(Name = "E-mail"), EmailAddress]
        public string FromEmail { get; set; }
        [Required]
        public string Message { get; set; }
    }
}