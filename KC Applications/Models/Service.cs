﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KC_Applications.Models {
    public class Service {
        public int ServiceId { get; set; }
        public string ServiceTitle { get; set; }
        public string ServiceDescription { get; set; }


        [Required]
        [StringLength(255)]
        [Display(Name = "First Name")]
        public string CustomerFirstName { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Last Name")]
        public string CustomerLastName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "E-mail")]
        public string CustomerEmail { get; set; }

        [Required]
        [Display(Name = "Contact Number (without area code)")]
        public int CustomerNumber { get; set; }


        [Display(Name = "Region to Work in")]
        public ServRegion ServiceArea { get; set; }
        [Display(Name = "Region to Work in")]
        [Required]
        public byte ServiceAreaId { get; set; }
    }
}