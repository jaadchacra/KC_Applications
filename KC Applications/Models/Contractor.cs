﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KC_Applications.Models {
    public class Contractor {

        public int ContractorId { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "First Name")]
        public string ContractorFirstName { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Last Name")]
        public string ContractorLastName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "E-mail")]
        public string ContractorEmail { get; set; }

        //[Required]
        [Display(Name = "Password")]
        public string ContractorPassword { get; set; }

        [Display(Name = "LinkedIn Page")]
        [Required]
        public string LinkedInPage { get; set; }

        public bool isContractorFreeToWork { get; set; }

        public bool isAccepted { get; set; }


        [Display(Name = "Your Field of Work")]
        public Expertise Expertise { get; set; }
        [Display(Name = "Your Field of Work")]
        [Required]
        public byte ExpertiseId { get; set; }


        [Display(Name = "Region to Work in")]
        public Region Region { get; set; }
        [Display(Name = "Region to Work in")]
        [Required]
        public byte RegionId { get; set; }
    }
}