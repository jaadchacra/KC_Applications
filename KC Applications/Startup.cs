﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KC_Applications.Startup))]
namespace KC_Applications
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
