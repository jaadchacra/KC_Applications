﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using KC_Applications.Models;
using KC_Applications.ViewModels;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KC_Applications.Controllers {
    public class ServiceofKCsController : Controller {
        private ApplicationDbContext _context;

        public ServiceofKCsController() {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing) {
            _context.Dispose();
        }


        // GET: Services
        public ActionResult Index() {
            if (User.IsInRole("Admin"))
                return View();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Income() {
            var DateNow = DateTime.Now;
            var DateStr = DateNow.ToString();
            var month = DateStr.Substring(3, 2);

            var services = _context.ServiceofKCs.ToList();
            List<ServiceofKC> serviceofKCs = new List<ServiceofKC> { };
            foreach (var service in services) {
                if (service.Status == "3") {
                    if (service.DateApproved.ToString().Substring(3, 2) == month) {
                        serviceofKCs.Add(service);
                    }
                }
            }
            if (User.IsInRole("Admin"))
                return View(serviceofKCs);
            return RedirectToAction("Index", "Home");
            
        }

        public ActionResult Edit(int id) {
            var serviceofKC = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);

            if (serviceofKC == null)
                return HttpNotFound();

            var viewModel = new ServiceofKCFormViewModel {
                ServiceofKC = serviceofKC,
                RegionofCustomers = _context.RegionofCustomers.ToList(),
            };
            if (User.IsInRole("Admin"))
                return View("EditForm", viewModel);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(ServiceofKC serviceofKC) {
            if (!ModelState.IsValid) {
                var viewModel = new ServiceofKCFormViewModel {
                    ServiceofKC = serviceofKC,
                    RegionofCustomers = _context.RegionofCustomers.ToList(),
                };
                //If Validation Error then reload the same form
                if (User.IsInRole("Admin"))
                    return View("EditForm", viewModel);
                return RedirectToAction("Index", "Home");
            }

            var serviceofKCInDb = _context.ServiceofKCs.Single(c => c.ServiceofKCId == serviceofKC.ServiceofKCId);
            serviceofKCInDb.RegionofCustomerId = serviceofKC.RegionofCustomerId;
            serviceofKCInDb.CustomerFirstName = serviceofKC.CustomerFirstName;
            serviceofKCInDb.CustomerLastName = serviceofKC.CustomerLastName;
            serviceofKCInDb.CustomerEmail = serviceofKC.CustomerEmail;
            serviceofKCInDb.CustomerNumber = serviceofKC.CustomerNumber;
            serviceofKCInDb.ServiceTitle = serviceofKC.ServiceTitle;
            serviceofKCInDb.ServiceDescription = serviceofKC.ServiceDescription;
            serviceofKCInDb.UrgencyDate = serviceofKC.UrgencyDate;
            serviceofKCInDb.Status = serviceofKC.Status;


            _context.SaveChanges();

            if (User.IsInRole("Admin"))
                return RedirectToAction("Index", "ServiceofKCs");
            return RedirectToAction("Index", "Home");

        }



        public ActionResult Contact(int id) {
            var serviceofKC = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);
            var emailFormModel = new EmailFormModel { FromEmail = serviceofKC.CustomerEmail, FromName = serviceofKC.CustomerFirstName + " " + serviceofKC.CustomerLastName };
            if (serviceofKC == null)
                return HttpNotFound();

            ViewBag.Message = "Your contact page.";

            return View(emailFormModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailFormModel model) {
            if (ModelState.IsValid) {
                var body = "<p>{2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(model.FromEmail));  // replace with valid value 
                message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
                message.Subject = "KC Applications";
                message.Body = string.Format(body, model.FromEmail, model.FromEmail, model.Message);
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient()) {
                    var credential = new NetworkCredential {
                        UserName = "jxc02dev@gmail.com",  // replace with valid value
                        Password = "Everlast**77"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            return View(model);
        }




        public ActionResult GenerateQuote(int id) {
            if (User.IsInRole("Admin"))
                return View();
            return RedirectToAction("Index", "Home");
            
        }



        public ActionResult ApproveService(int id) {
            var serviceofKC = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);
            if (User.IsInRole("Admin"))
                return View(serviceofKC);
            return RedirectToAction("Index", "Home");
            
        }

        public ActionResult InventoryIndex() {
            if (User.IsInRole("Admin"))
                return View();
            return RedirectToAction("Index", "Home");
        }




    }
}