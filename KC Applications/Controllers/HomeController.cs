﻿using KC_Applications.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using KC_Applications.ViewModels;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;


namespace KC_Applications.Controllers {
    [AllowAnonymous]
    public class HomeController : Controller {

        private ApplicationDbContext _context;

        public HomeController() {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing) {
            _context.Dispose();
        }


        public ActionResult Index() {
            if (User.IsInRole("Contractor"))
                return RedirectToAction("Financial", "Contractors");
            if (User.IsInRole("Admin"))
                return RedirectToAction("Index", "ServiceofKCs");
            if (User.IsInRole("Other"))
                return RedirectToAction("RequestSuccess", "Home");

            return View();
        }

        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult RequestSuccess() {
            return View();
        }

        [AllowAnonymous]
        public ActionResult CustomerOrder() {
            var regions = _context.Regions.ToList();
            var viewModel = new ContractorFormViewModel {
                Regions = regions,
            };

            return View(viewModel);
        }

        [AllowAnonymous]
        public ActionResult CheckRegion(string query = null) {
            if (String.IsNullOrWhiteSpace(query))
                return RedirectToAction("Index");

            var contractorsQuery = _context.Contractors.Include(c => c.Region).Include(c => c.Expertise);
            var service = new ServiceofKC {
                RegionofCustomerId = Convert.ToByte(query),
                DateRequested = DateTime.Now,
                Status = "1"
            };
            if (!String.IsNullOrWhiteSpace(query))
                contractorsQuery = contractorsQuery.Where(c => c.Region.Id.ToString().Contains(query));

            //Mapping from Contractor to ContractorDto (so we GET a ContractorDto)
            //Auto mapper allows us to map all the properties instead of explicitly doing each one
            var contractorDtos = contractorsQuery.ToList();

            if(contractorDtos.Count == 0) {
                return RedirectToAction("NoContractorInRegion");
            }
            return View(service);
        }

        [AllowAnonymous]
        public ActionResult NoContractorInRegion() {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RequestForOrder(ServiceofKC service) {
            var body = "<p>Email From: {0} ({1})</p><p>Title:</p><p>{2}</p><p>Description:</p><p>{3}</p>";
            var message = new MailMessage();
            if (!ModelState.IsValid) {
                if (service.ServiceofKCId == 0) {
                    _context.ServiceofKCs.Add(service);
                    await _context.SaveChangesAsync();



                    message.To.Add(new MailAddress("jxc02@mail.aub.edu"));  // replace with valid value 
                    message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
                    message.Subject = "New Service Request";
                    message.Body = string.Format(body, service.CustomerFirstName + " " + service.CustomerLastName, service.CustomerEmail, service.ServiceTitle, service.ServiceDescription);
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient()) {
                        var credential = new NetworkCredential {
                            UserName = "jxc02dev@gmail.com",  // replace with valid value
                            Password = "Everlast**77"  // replace with valid value
                        };
                        smtp.Credentials = credential;
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        await smtp.SendMailAsync(message);
                        return RedirectToAction("RequestSuccess", "Contractors");
                    }


                }

                //If Validation Error then reload the same form
                return View("CheckRegion", service);
            }

            _context.ServiceofKCs.Add(service);
            await _context.SaveChangesAsync();



            message.To.Add(new MailAddress("jxc02@mail.aub.edu"));  // replace with valid value 
            message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
            message.Subject = "New Service Request";
            message.Body = string.Format(body, service.CustomerFirstName+" "+service.CustomerLastName, service.CustomerEmail, service.ServiceTitle, service.ServiceDescription);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient()) {
                var credential = new NetworkCredential {
                    UserName = "jxc02dev@gmail.com",  // replace with valid value
                    Password = "Everlast**77"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
                return RedirectToAction("Sent");
            }

        }
        public ActionResult Sent() {
            return View();
        }
    }
}