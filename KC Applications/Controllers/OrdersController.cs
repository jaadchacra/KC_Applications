﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using KC_Applications.Models;
using KC_Applications.ViewModels;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KC_Applications.Controllers {
    public class OrdersController : Controller {


        private ApplicationDbContext _context;

        public OrdersController() {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing) {
            _context.Dispose();
        }



        // GET: Orders
        public ActionResult New(int id) {
            var serviceofKC = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);
            if (User.IsInRole("Admin"))
                return View(serviceofKC);
            return RedirectToAction("Index", "Home");

            
        }

        
        public async Task<ActionResult> EmailQuote(int id) {
            var serviceofKC = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);
            var inventoryFee = serviceofKC.InventoryPrice;
            var contractorFee = serviceofKC.ExpertisePrice;
            var totalPrice = inventoryFee + contractorFee;
            if (ModelState.IsValid) {
                var body = "<p><strong>Inventory Fee:</strong><p><p>"+ inventoryFee+" LBP </p>" +
                    "<p><strong>Contractor Fee:</strong><p><p>"+ contractorFee + " LBP</p>" +
                    "<p><strong>Total Price:</strong></p><p>"+ totalPrice+" LBP </p>" +
                     "<br></br>" +
                    "<p>Please Find above the quotation for your requested order.</p>" +
                    "<p>Kindly reply to this e-mail with an approval or rejection.</p>" +
                    "<br></br>" +
                    "<p>Sincerely,</p>" +
                    "<p>KC Applications</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(serviceofKC.CustomerEmail));  // replace with valid value 
                message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
                message.Subject = "Offer Quotation";
                message.Body = string.Format(body, "", "", "");
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient()) {
                    var credential = new NetworkCredential {
                        UserName = "jxc02dev@gmail.com",  // replace with valid value
                        Password = "Everlast**77"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            if (User.IsInRole("Admin"))
                return View();
            return RedirectToAction("Index", "Home");
            
        }


        public async Task<ActionResult> EmailApproved(int id) {
            var serviceofKC = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);
            var contractor = _context.Contractors.SingleOrDefault(c => c.ContractorId == serviceofKC.ContractorId);

            var customerFirstName = serviceofKC.CustomerFirstName;
            var customerLastName = serviceofKC.CustomerLastName;

            var contractorFirstName = contractor.ContractorFirstName;
            var contractorLastName = contractor.ContractorLastName;
            var contractorEmail = contractor.ContractorEmail;

            if (ModelState.IsValid) {
                var body = "<p>Dear "+ customerFirstName +" " + customerLastName  + ","+ "<p><p>" + contractorFirstName + " " + contractorLastName + " will be in contact with you shortly.</p>" +
                    "<p>If you wish to contact him before he does, please use the e-mail below:<p><p>" + contractorEmail + "</p>" +
                    "<br></br>"+"<p>Thank you for your business,</p>"+
                    "<p>KC Applications</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(serviceofKC.CustomerEmail));  // replace with valid value 
                message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
                message.Subject = "Offer Approval";
                message.Body = string.Format(body, "", "", "");
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient()) {
                    var credential = new NetworkCredential {
                        UserName = "jxc02dev@gmail.com",  // replace with valid value
                        Password = "Everlast**77"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("SentApproval");
                }
            }
            return View();
        }
        


        public ActionResult Sent() {
            return View();
        }

        public ActionResult SentApproval() {
            return View();
        }

    }
}