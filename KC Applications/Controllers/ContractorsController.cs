﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using KC_Applications.Models;
using KC_Applications.ViewModels;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KC_Applications.Controllers {
    public class ContractorsController : Controller {
        private ApplicationDbContext _context;

        public ContractorsController() {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing) {
            _context.Dispose();
        }
        // GET: Contractor
        public ActionResult Index() {
            if (User.IsInRole("Admin"))
                return View();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Edit(int id) {
            var contractor = _context.Contractors.SingleOrDefault(c => c.ContractorId == id);

            if (contractor == null)
                return HttpNotFound();

            var viewModel = new ContractorFormViewModel {
                Contractor = contractor,
                Regions = _context.Regions.ToList(),
                Expertises = _context.Expertises.ToList()
            };
            if (User.IsInRole("Admin"))
                return View("EditForm", viewModel);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Contractor contractor) {
            if (!ModelState.IsValid) {
                var viewModel = new ContractorFormViewModel {
                    Contractor = contractor,
                    Regions = _context.Regions.ToList(),
                    Expertises = _context.Expertises.ToList()
                };
                //If Validation Error then reload the same form
                if (User.IsInRole("Admin"))
                    return View("EditForm", viewModel);
                return RedirectToAction("Index", "Home");
            }

            if (contractor.ContractorId == 0)
                _context.Contractors.Add(contractor);
            else {
                var customerInDb = _context.Contractors.Single(c => c.ContractorId == contractor.ContractorId);
                customerInDb.ContractorFirstName = contractor.ContractorFirstName;
                customerInDb.ContractorLastName = contractor.ContractorLastName;
                customerInDb.ContractorEmail = contractor.ContractorEmail;
                customerInDb.LinkedInPage = contractor.LinkedInPage;
                customerInDb.ExpertiseId = contractor.ExpertiseId;
                customerInDb.RegionId = contractor.RegionId;
                customerInDb.isContractorFreeToWork = contractor.isContractorFreeToWork;

            }

            _context.SaveChanges();

            if (User.IsInRole("Admin"))
                return RedirectToAction("Index", "Contractors");
            return RedirectToAction("Index", "Home");

        }

        [AllowAnonymous]
        public ActionResult VacanciesForm() {
            var regions = _context.Regions.ToList();
            var expertises = _context.Expertises.ToList();
            var viewModel = new ContractorFormViewModel {
                Regions = regions,
                Expertises = expertises
            };

            return View(viewModel);
        }


        //THIS IS NOW IN REGISTER!! IN ACCOUNT CONTROLLER
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RequestForApproval(Contractor contractor) {
            var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
            var message = new MailMessage();
            if (!ModelState.IsValid) {
                if (contractor.ContractorId == 0) {
                    _context.Contractors.Add(contractor);
                    await _context.SaveChangesAsync();



                    message.To.Add(new MailAddress("jxc02@mail.aub.edu"));  // replace with valid value 
                    message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
                    message.Subject = "New Contractor Request";
                    message.Body = string.Format(body, "Contractor", "Contractor Email", "I would like to work with you");
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient()) {
                        var credential = new NetworkCredential {
                            UserName = "jxc02dev@gmail.com",  // replace with valid value
                            Password = "Everlast**77"  // replace with valid value
                        };
                        smtp.Credentials = credential;
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.EnableSsl = true;
                        await smtp.SendMailAsync(message);
                        return RedirectToAction("RequestSuccess", "Contractors");
                    }


                }

                var viewModel = new ContractorFormViewModel {
                    Contractor = contractor,
                    Regions = _context.Regions.ToList(),
                    Expertises = _context.Expertises.ToList()
                };
                //If Validation Error then reload the same form
                return View("VacanciesForm", viewModel);
            }

            _context.Contractors.Add(contractor);
            await _context.SaveChangesAsync();



            message.To.Add(new MailAddress("jxc02@mail.aub.edu"));  // replace with valid value 
            message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
            message.Subject = "New Contractor Request";
            message.Body = string.Format(body, "Contractor", "Contractor Email", "I would like to work with you");
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient()) {
                var credential = new NetworkCredential {
                    UserName = "jxc02dev@gmail.com",  // replace with valid value
                    Password = "Everlast**77"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
                return RedirectToAction("Sent");
            }

        }

        [AllowAnonymous]
        public ActionResult RequestSuccess() {
            return View();
        }





        public ActionResult Contact(int id) {
            var contractor = _context.Contractors.SingleOrDefault(c => c.ContractorId == id);
            var emailFormModel = new EmailFormModel { FromEmail = contractor.ContractorEmail, FromName = contractor.ContractorFirstName + " " + contractor.ContractorLastName };
            if (contractor == null)
                return HttpNotFound();



            ViewBag.Message = "Your contact page.";

            if (User.IsInRole("Admin"))
                return View(emailFormModel);
            return RedirectToAction("Index", "Home");

            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailFormModel model) {
            if (ModelState.IsValid) {
                var body = "<p>{2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(model.FromEmail));  // replace with valid value 
                message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
                message.Subject = "New Contractor Request From KC Applications";
                message.Body = string.Format(body, model.FromEmail, model.FromEmail, model.Message);
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient()) {
                    var credential = new NetworkCredential {
                        UserName = "jxc02dev@gmail.com",  // replace with valid value
                        Password = "Everlast**77"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            return View(model);
        }

        public ActionResult Sent() {
            return View();
        }

        public ActionResult Approve(int id) {
            var contractor = _context.Contractors.SingleOrDefault(c => c.ContractorId == id);

            if (contractor == null)
                return HttpNotFound();

            if (User.IsInRole("Admin"))
                return View(contractor);
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> EmailApproval(int id) {
            var customerInDb = _context.Contractors.Single(c => c.ContractorId == id);

            var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var user = um.FindByName(customerInDb.ContractorEmail);
            if (!um.IsInRole(user.Id, "Contractor"))
                um.AddToRole(user.Id, "Contractor");
            if (um.IsInRole(user.Id, "Other"))
                um.RemoveFromRole(user.Id, "Other");

            customerInDb.isAccepted = true;
            await _context.SaveChangesAsync();

            var body = "<p>Your account has been approved</p>  <p>Expect e-mails from us regarding contracting opportunities in your area</p>  <p>You can login using your e-mail and password to see your financial reports</p>  <p>Thank you for your business,</p>  <p>KC Applications</p>";
            var message = new MailMessage();
            message.To.Add(new MailAddress(customerInDb.ContractorEmail));  // replace with valid value 
            message.From = new MailAddress("jxc02dev@gmail.com");  // replace with valid value
            message.Subject = "Approval From KC Applications";
            message.Body = string.Format(body, "", "", "");
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient()) {
                var credential = new NetworkCredential {
                    UserName = "jxc02dev@gmail.com",  // replace with valid value
                    Password = "Everlast**77"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
            }


            if (User.IsInRole("Admin"))
                return RedirectToAction("Index", "Contractors");
            return RedirectToAction("Index", "Home");

        }


        public ActionResult Financial() {
            var jad = User.Identity.GetUserName();
            var contractorssQuery = _context.Contractors.Where(c => c.ContractorEmail == jad);
            var contractors = contractorssQuery.ToList();
            var id = 0;
            foreach (var contractor in contractors) {
                id = contractor.ContractorId;
            }
            var DateNow = DateTime.Now;
            var DateStr = DateNow.ToString();
            var month = DateStr.Substring(3, 2);

            var serviceofKCsQuery = _context.ServiceofKCs.Where(c => c.ContractorId == id);
            var services = serviceofKCsQuery.ToList();
            List<ServiceofKC> serviceofKCs = new List<ServiceofKC> { };
            foreach (var service in services) {
                if (service.DateApproved.ToString().Substring(3, 2) == month) {
                    serviceofKCs.Add(service);
                }
            }

            if (User.IsInRole("Contractor"))
                return View(serviceofKCs);
            return RedirectToAction("Index", "Home");
            
        }



    }
}

