﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using KC_Applications.Models; 
using KC_Applications.Dtos;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KC_Applications.Controllers.Api {
    public class ServiceofKCsController : ApiController {
        private ApplicationDbContext _context;

        public ServiceofKCsController() {
            _context = new ApplicationDbContext();
        }


        // GET /api/serviceofKCs
        public IHttpActionResult GetServiceofKCs(string query = null) {
            //This gets ALL ServiceofKCs from DB
            //It's also EAGERLOADING Region!!!! to be able to use in view from the response
            var serviceofKCsQuery = _context.ServiceofKCs.Include(c => c.RegionofCustomer);

            if (!String.IsNullOrWhiteSpace(query))
                serviceofKCsQuery = serviceofKCsQuery.Where(c => c.RegionofCustomer.RegionofCustomerName.Contains(query));

            //Mapping from ServiceofKC to ServiceofKCDto (so we GET a ServiceofKCDto)
            //Auto mapper allows us to map all the properties instead of explicitly doing each one
            var serviceofKCDtos = serviceofKCsQuery.ToList().Select(Mapper.Map<ServiceofKC, ServiceofKCDto>);

            return Ok(serviceofKCDtos);
        }

        // GET /api/serviceofKCs/1
        public IHttpActionResult GetServiceofKC(int id) {

            //This gets a ServiceofKC from DB that matches given id parameter
            var serviceofKC = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);

            // If serviceofKC doesn't exist return not found
            if (serviceofKC == null)
                return NotFound();

            //Otherwise return ServiceofKCDto by Mapping ServiceofKC to ServiceofKCDto
            return Ok(Mapper.Map<ServiceofKC, ServiceofKCDto>(serviceofKC));
        }

        // POST /api/serviceofKCs
        [HttpPost]
        public IHttpActionResult CreateServiceofKC(ServiceofKCDto serviceofKCDto) {
            if (!ModelState.IsValid)
                return BadRequest();

            //Mapping from ServiceofKCDto to ServiceofKC (so we POST a ServiceofKC)
            var serviceofKC = Mapper.Map<ServiceofKCDto, ServiceofKC>(serviceofKCDto);
            // Add ServiceofKC to database and save changes
            _context.ServiceofKCs.Add(serviceofKC);
            _context.SaveChanges();

            // Since we added a ServiceofKC to our database, the database auto-generates an ID for it
            // We use that ID to give it to the ServiceofKCDto in our client (otherwise it won't have the same ID)
            serviceofKCDto.ServiceofKCId = serviceofKC.ServiceofKCId;

            //We return Creater Uri since we're using IHttpActionResult(see top for description)
            // It should be: /api/serviceofKCs/id
            return Created(new Uri(Request.RequestUri + "/" + serviceofKC.ServiceofKCId), serviceofKCDto);
        }

        // PUT /api/serviceofKCs/1
        [HttpPut]
        public IHttpActionResult UpdateServiceofKC(int id, ServiceofKCDto serviceofKCDto) {
            if (!ModelState.IsValid)
                return BadRequest();

            var serviceofKCInDb = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);

            if (serviceofKCInDb == null)
                return NotFound();

            //Notice that since we already have var serviceofKCInDb, we just instantly called the Mapping function
            Mapper.Map(serviceofKCDto, serviceofKCInDb);

            _context.SaveChanges();

            return Ok();
        }

        // DELETE /api/serviceofKCs/1
        [HttpDelete]
        public IHttpActionResult DeleteServiceofKC(int id) {
            //Get serviceofKC from db, if not found then can't delete
            var serviceofKCInDb = _context.ServiceofKCs.SingleOrDefault(c => c.ServiceofKCId == id);

            if (serviceofKCInDb == null)
                return NotFound();

            _context.ServiceofKCs.Remove(serviceofKCInDb);
            _context.SaveChanges();

            return Ok();
        }





    }
}
