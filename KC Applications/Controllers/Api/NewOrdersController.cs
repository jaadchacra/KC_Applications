﻿using System;
using System.Linq;
using System.Web.Http;
using KC_Applications.Dtos;
using KC_Applications.Models;

namespace KC_Applications.Controllers.Api {
    public class NewOrdersController : ApiController {
        private ApplicationDbContext _context;

        public NewOrdersController() {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult CreateNewOrders(NewOrderDto newOrder) {
            var serviceofKC = _context.ServiceofKCs.Single(c => c.ServiceofKCId == newOrder.ServiceofKCId);
            serviceofKC.Status = "2";
            var Expertice = _context.Expertises.Single(c => c.Id == newOrder.ExpertiseId);
            serviceofKC.ExpertisePrice = Expertice.ExpertisePrice; 

            foreach (var id in newOrder.LocalInventoryIds) {
                var inventory = _context.LocalInventories.Single(c => c.Id == id);
                if (inventory.NumberInStock == 0)
                    return BadRequest("Inventory is not available.");
                var order = new Order {
                    ServiceofKC = serviceofKC,
                    LocalInventory = inventory,
                };

                serviceofKC.InventoryPrice += inventory.Price;
                serviceofKC.InventoryCOG += inventory.COG;

                _context.Orders.Add(order);

            }

            _context.SaveChanges();

            return Ok();
        }


    }
}
