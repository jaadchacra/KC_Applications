﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using KC_Applications.Models;
using KC_Applications.Dtos;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

/* We have 2 GETS (one for ALL Contractors and one for 1 Contractor with id parameter)
 * We have 1 POST (for 1 Contractor)
 * We have 1 PUT (for 1 Contractor)
 * We have 1 DELETE (for 1 Contractor)
 */
/*PS: We're returning IHttpActionResult BECAUSE apiController needs a 202 response not 200 response for OK
 (it's similar to ActionResult in MVC Controller)
 */
namespace KC_Applications.Controllers.api {
    public class ContractorsController : ApiController {
        private ApplicationDbContext _context;

        public ContractorsController() {
            _context = new ApplicationDbContext();
        }

        // GET /api/contractors
        public IHttpActionResult GetContractors(string query = null) {
            //This gets ALL Contractors from DB
            //It's also EAGERLOADING Region!!!! to be able to use in view from the response
            var contractorsQuery = _context.Contractors.Include(c => c.Region).Include(c => c.Expertise);

            if (!String.IsNullOrWhiteSpace(query))
                contractorsQuery = contractorsQuery.Where(c => c.Region.RegionName.Contains(query));

            //Mapping from Contractor to ContractorDto (so we GET a ContractorDto)
            //Auto mapper allows us to map all the properties instead of explicitly doing each one
            var contractorDtos = contractorsQuery.ToList().Select(Mapper.Map<Contractor, ContractorDto>);

            return Ok(contractorDtos);
        }

        // GET /api/contractors/1
        public IHttpActionResult GetContractor(int id) {

            //This gets a Contractor from DB that matches given id parameter
            var contractor = _context.Contractors.SingleOrDefault(c => c.ContractorId == id);

            // If contractor doesn't exist return not found
            if (contractor == null)
                return NotFound();

            //Otherwise return ContractorDto by Mapping Contractor to ContractorDto
            return Ok(Mapper.Map<Contractor, ContractorDto>(contractor));
        }

        // POST /api/contractors
        [HttpPost]
        public IHttpActionResult CreateContractor(ContractorDto contractorDto) {
            if (!ModelState.IsValid)
                return BadRequest();

            //Mapping from ContractorDto to Contractor (so we POST a Contractor)
            var contractor = Mapper.Map<ContractorDto, Contractor>(contractorDto);
            // Add Contractor to database and save changes
            _context.Contractors.Add(contractor);
            _context.SaveChanges();

            // Since we added a Contractor to our database, the database auto-generates an ID for it
            // We use that ID to give it to the ContractorDto in our client (otherwise it won't have the same ID)
            contractorDto.ContractorId = contractor.ContractorId;

            //We return Creater Uri since we're using IHttpActionResult(see top for description)
            // It should be: /api/contractors/id
            return Created(new Uri(Request.RequestUri + "/" + contractor.ContractorId), contractorDto);
        }

        // PUT /api/contractors/1
        [HttpPut]
        public IHttpActionResult UpdateContractor(int id, ContractorDto contractorDto) {
            if (!ModelState.IsValid)
                return BadRequest();

            var contractorInDb = _context.Contractors.SingleOrDefault(c => c.ContractorId == id);

            if (contractorInDb == null)
                return NotFound();

            //Notice that since we already have var contractorInDb, we just instantly called the Mapping function
            Mapper.Map(contractorDto, contractorInDb);

            _context.SaveChanges();

            return Ok();
        }

        // DELETE /api/contractors/1
        [HttpDelete]
        public IHttpActionResult DeleteContractor(int id) {
            //Get contractor from db, if not found then can't delete
            var contractorInDb = _context.Contractors.SingleOrDefault(c => c.ContractorId == id);

            var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            var user = um.FindByName(contractorInDb.ContractorEmail);
            if (!um.IsInRole(user.Id, "Other"))
                um.AddToRole(user.Id, "Other");
            if (um.IsInRole(user.Id, "Contractor"))
                um.RemoveFromRole(user.Id, "Contractor");

            if (contractorInDb == null)
                return NotFound();

            _context.Contractors.Remove(contractorInDb);
            _context.SaveChanges();

            return Ok();
        }
    }
}
