﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using KC_Applications.Models;
using KC_Applications.Dtos;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KC_Applications.Controllers.Api {
    public class AssignContractorController : ApiController {
        private ApplicationDbContext _context;

        public AssignContractorController() {
            _context = new ApplicationDbContext();
        }

        // GET /api/contractors
        public IHttpActionResult GetContractors(string query = null) {
            //This gets ALL Contractors from DB
            //It's also EAGERLOADING Region!!!! to be able to use in view from the response
            var contractorsQuery = _context.Contractors.Include(c => c.Region).Include(c => c.Expertise);

            if (!String.IsNullOrWhiteSpace(query))
                contractorsQuery = contractorsQuery.Where(c => c.ContractorFirstName.Contains(query));

            //Mapping from Contractor to ContractorDto (so we GET a ContractorDto)
            //Auto mapper allows us to map all the properties instead of explicitly doing each one
            var contractorDtos = contractorsQuery.ToList().Select(Mapper.Map<Contractor, ContractorDto>);

            return Ok(contractorDtos);
        }
    }
}
