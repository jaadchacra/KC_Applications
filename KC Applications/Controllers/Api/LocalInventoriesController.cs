﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using KC_Applications.Dtos;
using KC_Applications.Models;

namespace KC_Applications.Controllers.Api {
    public class LocalInventoriesController : ApiController {

        private ApplicationDbContext _context;

        public LocalInventoriesController() {
            _context = new ApplicationDbContext();
        }

        public IEnumerable<LocalInventoryDto> GetLocalInventories(string query = null) {
            var inventoriesQuery = _context.LocalInventories.Where(m => m.NumberInStock > 0);

            if (!String.IsNullOrWhiteSpace(query))
                inventoriesQuery = inventoriesQuery.Where(m => m.LocalInventoryName.Contains(query));

            return inventoriesQuery.ToList().Select(Mapper.Map<LocalInventory, LocalInventoryDto>);
        }

        public IHttpActionResult GetMovie(int id) {
            var inventory = _context.LocalInventories.SingleOrDefault(c => c.Id == id);

            if (inventory == null)
                return NotFound();

            return Ok(Mapper.Map<LocalInventory, LocalInventoryDto>(inventory));
        }



    }
}
