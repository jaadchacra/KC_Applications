﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using KC_Applications.Models;
using KC_Applications.Dtos;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KC_Applications.Controllers.Api {
    public class ApproveOrdersController : ApiController {
        private ApplicationDbContext _context;

        public ApproveOrdersController() {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult CreateNewOrders(ApproveOrderDto approveOrder) {
            var serviceofKC = _context.ServiceofKCs.Single(c => c.ServiceofKCId == approveOrder.ServiceofKCId);
            serviceofKC.Status = "3";
            serviceofKC.ContractorId = approveOrder.ContractorId;
            serviceofKC.DateApproved = DateTime.Now;

            var ordersQuery = _context.Orders.Include(c => c.ServiceofKC).Include(c => c.LocalInventory);
            ordersQuery = ordersQuery.Where(c => c.ServiceofKC.ServiceofKCId == serviceofKC.ServiceofKCId);

            var orders = ordersQuery.ToList();

            foreach (var order in orders){
                
                var inventory = _context.LocalInventories.Single(c => c.Id == order.LocalInventory.Id);

                if (inventory.NumberInStock != 9999999) {
                    inventory.NumberInStock--;
                }

            }



                _context.SaveChanges();

            return Ok();
        }


    }
}
