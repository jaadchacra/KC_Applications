﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KC_Applications.Dtos {
    public class LocalInventoryDto {
        public byte Id { get; set; }

        public string LocalInventoryName { get; set; }

        public double Price { get; set; }

        public double COG { get; set; }

        public int NumberInStock { get; set; }
    }
}