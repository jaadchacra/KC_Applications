﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KC_Applications.Dtos {
    public class NewOrderDto {
        public int ServiceofKCId { get; set; }
        public List<int> LocalInventoryIds { get; set; }
        public int ExpertiseId { get; set; }
    }
}