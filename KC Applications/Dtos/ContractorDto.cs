﻿using KC_Applications.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

//Dtos are safer than Models when operating on the client side
//They also have a function other than security but that is our of our scope
namespace KC_Applications.Dtos {
    public class ContractorDto {
        public int ContractorId { get; set; }

        [Required]
        [StringLength(255)]
        public string ContractorFirstName { get; set; }

        //[Required]
        [StringLength(255)]
        public string ContractorLastName { get; set; }

        //[Required]
        public string ContractorEmail { get; set; }

        //[Required]
        public string ContractorPassword { get; set; }

        //[Required]
        public string LinkedInPage { get; set; }

        public bool isContractorFreeToWork { get; set; }

        public bool isAccepted { get; set; }

        public ExpertiseDto Expertise { get; set; }
        public byte ExpertiseId { get; set; }

        public RegionDto Region { get; set; }
        public byte RegionId { get; set; }
    }

}