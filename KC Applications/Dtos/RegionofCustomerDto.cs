﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KC_Applications.Dtos {
    public class RegionofCustomerDto {
        public byte RegionofCustomerId { get; set; }

        public string RegionofCustomerName { get; set; }
    }
}