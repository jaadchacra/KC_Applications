﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KC_Applications.Dtos {
    public class RegionDto {
        public byte Id { get; set; }
        public string RegionName { get; set; }
    }
}