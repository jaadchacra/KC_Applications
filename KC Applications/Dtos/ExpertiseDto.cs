﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KC_Applications.Dtos {
    public class ExpertiseDto {
        public byte Id { get; set; }
        public string ExpertiseName { get; set; }
        public double ExpertisePrice { get; set; }
    }
}