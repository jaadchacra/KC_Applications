﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KC_Applications.Dtos {
    public class ApproveOrderDto {
        public int ServiceofKCId { get; set; }
        public int ContractorId { get; set; }
    }
}